#!/usr/bin/perl

# Add as cron job to run every minute, pull values for display from mysql
# Make sure to change Serial port location to the right /dev/ and to 
# change the database connection values to match the setup on the pi
# its currently looking for a schema named 'lightr' with a table called 'readings'
# with three fields in the table called ftemp, ctemp, and light to hold the corresponding
# values. I Also set up my test DB with a timestamp field that autopopulates with the current
# timestamp. To display values I would suggest just pulling them right from the DB. Let me know if
# that proves difficult and I can write um out to a txt file to pull from. 

use warnings;

use DBI;
use Device::SerialPort;

#Connect to the Serial Port

my $port = Device::SerialPort->new("/dev/ttyUSB0"); #Change to proper location for Arduino
$port->databits(8);
$port->baudrate(9600);
$port->parity("none");
$port->stopbits(1);

# Set up variables to read serial port

my $var="2";
my $test="0";
my $ftemp="0";
my $ctemp="0";
my $light="0";

# Send Signal to Arduino to print info

my $return=$port->write("$var");

# Read off the Arduino, first reading is ignored to flush crap data, 
# 2nd reading is F Temp, 3rd reading is C Temp, 4th reading is light sensor

while (1) {
    my $char = $port->lookfor();
    if ($char) {
	$test +=1;
		if ($test == 2)
		{
        	$ftemp = $char;
		}
		if ($test == 3)
        	{
        	$ctemp = $char;
        	}
        	if ($test == 4)
        	{
        	$light = $char;
		last;
        	}
	}
    }

# Setup the Database Connection (Replace Schema name, user, and password)

$dbh = DBI->connect('dbi:mysql:lightr','root','admin123')
 or die "Connection Error: $DBI::errstr\n";

# Setup the query 

$sql = "INSERT INTO readings (ftemp, ctemp, light) VALUES ($ftemp, $ctemp, $light)";
$sth = $dbh->prepare($sql);

# Execute Query

$sth->execute
 or die "SQL Error: $DBI::errstr\n";
