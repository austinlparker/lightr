int light_one = 0;
int light_one_on = 4;
int light_one_off = 2;
int tempSense = A0;
int photoSense = A1;

void setup()
{
 // Open serial communications to PC and wait for port to open:
  Serial.begin(9600);

  Serial.println("Connected to PC");
  
  pinMode(light_one_on, OUTPUT);
  pinMode(light_one_off, OUTPUT);


}

void loop() // run over and over
{
  if (Serial.available()) {
   light_one = Serial.read();
   Serial.println(light_one);
   
   if (light_one == 49){
      digitalWrite(light_one_on, HIGH);
      delay(75);
      digitalWrite(light_one_on, LOW);

   }
    if (light_one == 48){
      digitalWrite(light_one_off, HIGH);
      delay(75);
      digitalWrite(light_one_off, LOW);
   }

   }
     int temp = analogRead(tempSense);
     float voltage = temp * 5.0;
     voltage /= 1024.0;
     float tempC = (voltage - 0.5) * 100;
     float tempF = (tempC * 9.0 / 5.0) + 32.0;
     
     if (light_one == 50)
     {
     delay (50);
     Serial.print(tempF); Serial.write("\n"); 
     Serial.print(tempC); Serial.write("\n"); 
     Serial.print(analogRead(photoSense)); Serial.write("\n");
     light_one = 0;
   }
}
