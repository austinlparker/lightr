<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="img/favicon.ico">

    <title>lightr</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">


<?php
$con=mysqli_connect("localhost","root","admin123","lightr");
if (mysqli_connect_errno()) {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
}
$result = mysqli_query($con,"select * from readings order by date desc limit 0,1");
while($row = mysqli_fetch_array($result)) {

echo "<script type=\"text/javascript\">";
echo "function insertVals(){" ;   
echo "  document.getElementById('currTemp').innerHTML=".$row['ftemp'].";";
echo "  document.getElementById('lightSense').innerHTML=".$row['light'].";";
echo "}";
echo "</script>";

}
mysqli_close($con);
?>


    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body onload="insertVals()">

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">lightr</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Settings</a></li>
            <li><a href="#">Profile</a></li>
            <li><a href="#">Help</a></li>
          </ul>
        </div>
      </div>
    </div>
    
   
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul id= "sidenav" class="nav nav-sidebar">
            <li class="active"><a href="#">Current Conditions</a></li>
            <li><a href="#">Temperature History</a></li>
          </ul>
        </div>
        <div id="currCon">
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Current Conditions</h1>

          <div class="row">
            <div class="col-xs-12 col-sm-3">
            <h1 id="currTemp">$TEMP</h1>
              <h4>Current Temperature</h4>
              <span id="currTime" class="text-muted">$TIME</span>
            </div>

            <div class="col-xs-12 col-sm-3">
            <h1 id="lightSense">$LSENSOR</h1>
              <h4>Light Sensor Status</h4>
            </div>

            <div class="col-xs-12 col-sm-3">
            <form action="php/on.php">
              <button id="lightStatus" type="button" class="btn btn-danger btn-large">
              <span id="light1statusicon" class="glyphicon glyphicon-off"></span> 1</button>
            <input id="toggleLight1" type="submit" value="Toggle">
            </form>
              <h4>Active Lights</h4>
            </div>
          </div>
          </div>
          </div>
          <div id="tempTable">

          <div class="row">
          <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                      <h1 class="page-header">Temperature History</h1>

                   <table cellspacing = "1">
                   <thead>
                      <tr>
                   <th>Date/Time</th>
                   <th>Temperature</th>
                      </tr>
                    </thead>

                    <tbody>

		   <?php
			$con=mysqli_connect("localhost","root","admin123","lightr");
			if (mysqli_connect_errno()) {
			  echo "Failed to connect to MySQL: " . mysqli_connect_error();
			}
			$result = mysqli_query($con,"select date, ftemp from readings order by date desc limit 0,30");
			while($row = mysqli_fetch_array($result)) {
                   echo '   <tr>';
                   echo '     <td>'.$row['date'].'..........</td>';
                   echo '     <td>'.$row['ftemp'].'</td>';
                   echo '   </tr>';
			}
			mysqli_close($con);
		   ?>
                    </tbody>
                    </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>
