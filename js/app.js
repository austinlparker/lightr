$(document).ready(function() { 
	$('#tempTable').hide();
	$('#currTime').each(function() {
		$(this).html(new Date($.now()).toLocaleString());
	});
	//convertLStoString();
});

$('#sidenav li').on('click', function() {
	$('#sidenav li').toggleClass('active');
	$('#currCon').toggle();
	$('#tempTable').toggle();
});

$('#toggleLight1').on('click', function() {
	event.preventDefault();
	if ($('#lightStatus').hasClass('btn-danger')) {
		$.ajax({url: 'php/on.php' }); 
	} else {
		($.ajax({url: 'php/off.php'}));
	}

	$('#lightStatus').toggleClass('btn-success btn-danger');
});

function convertLStoString() {
	var lcontainer = $('#lightSense2');
	var lstatus = ($('#lightSense').html());
	console.log(lstatus);
	if (lstatus >= 600) {
		lcontainer.html('Bright');
	} else if (lstatus >= 1000) {
		lcontainer.html('Very Bright!');
	} else {
		lcontainer.html('Not So Bright');
	}
}
